var express = require('express')
var app = express();
var http = require('http');
app.set('port', (process.env.PORT ||8080));
app.use(express.static(__dirname + '/public'));

app.on('uncaughtException', function(err) {
  console.log(err); 
});

app.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'OPTIONS,GET,POST,PUT,DELETE');
    res.header("Access-Control-Allow-Headers", "Content-Type, Authorization, X-Requested-With");
    if ('OPTIONS' == req.method){
        return res.send(200);
    }
    next();
});

/*app.get('/', function(request, response) {
  response.send('Hello World!')
})*/

app.get('/graphs/:gn/select/notin/:list', function(req, res) {
  var obj = JSON.parse(req.params.list);
  var script = [];
  obj.forEach(function(o) {
    script.push(o);
  });
  console.log(req.params.list, obj, script);
  var path = '/graphs/'+req.params.gn+'/tp/gremlin?script=g.V.has("type",T.notin,'+JSON.stringify(script).split(" ").join("%20")+').type.order';
  var path2 = path+".bothE";
  var opt = {
    host: 'localhost',
    port: 8182,
    path: path,
    method: "get"
  }
  console.log(path);
  var myData = [];
    var httpreq = http.get(opt, function (response) {
    response.setEncoding('utf8');
    response.on('data', function (chunk) {
      var result = JSON.parse(chunk).results;
      myData.push(result[0]);
      result.forEach(function(r) {
        if (r !== myData[myData.length-1])
          myData.push(r);

      });
    });
    response.on('end', function() {
      var opt2 = {
        host: 'localhost',
        port: 8182,
        path: path2,
        method: 'GET'
      }
      console.log(path2);
      var httpreq2 = http.get(opt2, function(response1) {
        response1.setEncoding('utf8');
        response1.on('data', function(chunk) {
          myData["links"] = JSON.parse(chunk).results;
        });
        response1.on('end', function() {
          res.send(myData);
        });
      });
      httpreq2.end();
    });
  });
  httpreq.end(); 
})
app.get('/graphs/:gn/vertices/notin', function(req,res) {
  var parapara = '[{"location":"32.67 : 22.87","name":"derna","type":"port","_id":2304,"_type":"vertex"},{"location":"31.19 : 29.92","name":"alexandria","type":"port","_id":2048,"_type":"vertex"},{"location":"38.24 : 19.97","name":"siderno","type":"port","_id":3328,"_type":"vertex"},{"location":"37.97 : 23.71","name":"athens","type":"port","_id":2560,"_type":"vertex"},{"location":"36.56 : 18.45","name":"catania","type":"port","_id":3072,"_type":"vertex"},{"location":"36.45 : 21.23","name":"pirgos","type":"port","_id":2816,"_type":"vertex"}]'
  var obj = JSON.parse(parapara);
  var list = [];
  obj.forEach(function(o) {
    list.push(o._id);
  })
  console.log(list);
  list = JSON.stringify(list);
  list = list.replace("[","[(long)").split(",").join(",(long)").split(" ").join("%20");
  var script = "g.V.has('id',T.notin,"+list+")"
  var path = "/graphs/"+req.params.gn+"/tp/gremlin?script="+script; 
  console.log(path);
  var opt = {
    host: 'localhost',
    port: 8182,
    path: path,
    method: 'GET'
  };
  var myData = [];
  var httpreq = http.get(opt, function (response) {
    response.setEncoding('utf8');
    response.on('data', function (chunk) {
      //console.log("body: " + chunk);
      myData = JSON.parse(chunk).results;
      // console.log(myData.nodes);
    });
    response.on('end', function() {
      res.send(myData);
    });
  });
  httpreq.end();
})

app.get('/graphs/:gn/vertices/:key', function(req, res) {
  var path = '/graphs/'+req.params.gn+'/tp/gremlin?script=g.V.'+req.params.key+'.order';
	console.log('sadlkaflk: '+path);
  var opt = {
    host: 'localhost',
    port: 8182,
    path: path,
    method: 'GET'
  };
  var myData = [];
  var httpreq = http.get(opt, function(response) {
    response.setEncoding('utf8');
    response.on('data', function(chunk) {
      var result = JSON.parse(chunk).results;
console.log('results: '+path);      
myData.push(result[0]);
      result.forEach(function(r) {
        if (r !== myData[myData.length-1])
          myData.push(r);

      });
    });
    response.on('end', function() {
      res.send(myData);
    });
  });
  httpreq.end();
})

app.get('/graphs/:gn/select/:list', function(req, res) {
  var obj = JSON.parse(req.params.list);
  var script = [];
  obj.forEach(function(o) {
    script.push(o);
  });
  console.log(req.params.list, obj, script);
  var splitted = JSON.stringify(script);
  splitted = splitted.split(" ").join("%20");
  var path = '/graphs/'+req.params.gn+'/tp/gremlin?script=g.V.has("type",T.in,'+splitted+')';
  console.log(path);
  var path2 = path+".bothE";
  var opt = {
    host: 'localhost',
    port: 8182,
    path: path,
    method: "get"
  }
  console.log(path);
  var myData = {"nodes": [], "links": []};
    var httpreq = http.get(opt, function (response) {
    response.setEncoding('utf8');
    response.on('data', function (chunk) {
      //console.log("body: " + chunk);
      myData["nodes"] = JSON.parse(chunk).results;
      //console.log(myData.nodes);
    });
    response.on('end', function() {
      var opt2 = {
        host: 'localhost',
        port: 8182,
        path: path2,
        method: 'GET'
      }
      console.log(path2);
      var httpreq2 = http.get(opt2, function(response1) {
        response1.setEncoding('utf8');
        response1.on('data', function(chunk) {
          myData["links"] = JSON.parse(chunk).results;
        });
        response1.on('end', function() {
          res.send(myData);
        });
      });
      httpreq2.end();
    });
  });
  httpreq.end(); 
});

app.get('/graphs/:gn/vertices/:key/:value', function(req, res) {
  var path = '/graphs/'+req.params.gn+'/vertices?key='+req.params.key+'&value='+req.params.value.split(" ").join("%20");
  var path2 = "/graphs/"+req.params.gn+"/tp/gremlin?script=g.V().has('"+req.params.key+"','"+req.params.value.split(" ").join("%20")+"').bothE";
  var opt1 = {
    host: 'localhost',
    port: 8182,
    path: path,
    method: 'GET'
  };
  var myData = {"nodes": [], "links": []};
  // var httpreq = http.get(opt, function (response) {
  //   response.setEncoding('utf8');
  //   response.on('data', function (chunk) {
  //     console.log("body: " + chunk);
  //     myData.nodes = JSON.parse(chunk).results;
  //     console.log(myData.nodes);
  //   });
  //   response.on('end', function() {
  //     res.send(myData);
  //   });
  // });
  var httpreq = http.get(opt1, function (response) {
    response.setEncoding('utf8');
    response.on('data', function (chunk) {
      console.log("body: " + chunk);
      myData["nodes"] = JSON.parse(chunk).results;
      //console.log(myData.nodes);
    });
    response.on('end', function() {
      var opt2 = {
        host: 'localhost',
        port: 8182,
        path: path2,
        method: 'GET'
      }
      console.log(path2);
      var httpreq2 = http.get(opt2, function(response1) {
        response1.setEncoding('utf8');
        response1.on('data', function(chunk) {
          myData["links"] = JSON.parse(chunk).results;
        });
        response1.on('end', function() {
          res.send(myData);
        });
      });
      httpreq2.end();
    });
  });
  httpreq.end(); 

});

app.get('/graphs/:gn/vertices', function(req, res) {
  var path = '/graphs/'+req.params.gn+'/vertices';
  var opt = {
    host: 'localhost',
    port: 8182,
    path: path,
    method: 'GET'
  };
  var myData = {nodes: [], links: []};
  var httpreq = http.get(opt, function (response) {
    response.setEncoding('utf8');
    response.on('data', function (chunk) {
      console.log("body: " + chunk);
      myData.nodes = JSON.parse(chunk).results;
      console.log(myData.nodes);
    });
    response.on('end', function() {
      res.send(myData);
    });
  });
  httpreq.end();

});

app.get('/graphs/:gn/edges', function(req, res) {
  var path = '/graphs/'+req.params.gn+'/edges';
  var opt = {
    host: 'localhost',
    port: 8182,
    path: path,
    method: 'GET'
  };
  var myData = [];
  var httpreq = http.get(opt, function (response) {
    response.setEncoding('utf8');
    response.on('data', function (chunk) {
      console.log("body: " + chunk);
      myData = JSON.parse(chunk).results;
      // console.log(myData.nodes);
    });
    response.on('end', function() {
      res.send(myData);
    });
  });
  httpreq.end();

});

app.get('/graphs/:gn/vertex/:id/edges', function(req, res) {
  var path = '/graphs/'+req.params.gn+'/vertices/'+req.params.id+'/bothE';
  var opt = {
    host: 'localhost',
    port: 8182,
    path: path,
    method: 'GET'
  };
  var myData = [];
  var httpreq = http.get(opt, function (response) {
    response.setEncoding('utf8');
    response.on('data', function (chunk) {
      console.log("body: " + chunk);
      myData = JSON.parse(chunk).results;
      // console.log(myData.nodes);
    });
    response.on('end', function() {
      res.send(myData);
    });
  });
  httpreq.end();

});

app.get('/graphs',function(req,res) {
  var opt = {
    host: 'localhost',
    port: 8182,
    path: '/graphs',
    method: 'GET'
  };
  var myData = [];
  var httpreq = http.get(opt, function(response) {
    response.setEncoding('utf8');
    response.on('data', function(chunk) {
      console.log("body: "+chunk);
      myData = JSON.parse(chunk).graphs;
    });
    response.on('end', function() {
      res.send(myData);
    })
  });
  httpreq.end();
});

app.get('/', function(req, res) {

  var opt1 = {
    host: 'localhost',
    port: 8182,
    path: '/graphs/BorderControl/vertices',
    method: 'GET'
  };
  var opt2 = {
    host: 'localhost',
    port: 8182,
    path: '/graphs/BorderControl/edges',
    method: 'GET'
  };
	var allDone = 0;
  var myData = {nodes: [], links: []};

  var httpreq = http.get(opt1, function (response) {
    response.setEncoding('utf8');
    response.on('data', function (chunk) {
      console.log("body: " + chunk);
      myData.nodes = JSON.parse(chunk).results;
      console.log(myData.nodes);
    });
    response.on('end', function() {
    	var httpreq2 = http.get(opt2, function(response1) {
		response1.setEncoding('utf8');
		response1.on('data', function(chunk) {
			myData.links = JSON.parse(chunk).results;
		});
		response1.on('end', function() {
			res.send(myData);
		});
	});
	httpreq2.end();
    });
  });
  httpreq.end();
});

app.get('/graphs/:gn', function(req, res) {

  var opt1 = {
    host: 'localhost',
    port: 8182,
    path: '/graphs/'+req.params.gn+'/vertices',
    method: 'GET'
  };
  var opt2 = {
    host: 'localhost',
    port: 8182,
    path: '/graphs/'+req.params.gn+'/edges',
    method: 'GET'
  };
  var allDone = 0;
  var myData = {nodes: [], links: []};

  var httpreq = http.get(opt1, function (response) {
    response.setEncoding('utf8');
    response.on('data', function (chunk) {
      console.log("body: " + chunk);
      myData.nodes = JSON.parse(chunk).results;
      console.log(myData.nodes);
    });
    response.on('end', function() {
      var httpreq2 = http.get(opt2, function(response1) {
    response1.setEncoding('utf8');
    response1.on('data', function(chunk) {
      myData.links = JSON.parse(chunk).results;
    });
    response1.on('end', function() {
      res.send(myData);
    });
  });
  httpreq2.end();
    });
  });
  httpreq.end();
});

app.listen(app.get('port'), function() {
  console.log("Node app is running at localhost:" + app.get('port'))
});
app.on('uncaughtException', function(err) {
console.log(err); });
